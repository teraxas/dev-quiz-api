const express = require('express');
const bodyParser = require('body-parser');
const e_question = require('./endpoint/question');
const e_user = require('./endpoint/user');

const app = express();
app.use(bodyParser.json());

const server = app.listen(process.env.PORT || 8080, function () {
  var port = server.address().port;
  var localAddress = 'http://localhost:' + port;
  console.log('DEV QUIZ API listening at %s', localAddress);

  console.log('\nRegistered endpoints');
  app._router.stack.forEach(function (r) {
    if (r.route && r.route.path) console.log(extractMethods(r.route) + ' ' + localAddress + r.route.path)
  })
  console.log('\n');
})

app.all('/*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", process.env.ALLOWED_ORIGIN || "http://localhost:3000");
  res.header("Access-Control-Allow-Headers", "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept");
  res.header("Access-Control-Allow-Credentials", "true")
  res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS")
  next();
});

function extractMethods(route) {
  const result = [];
  if (route.methods.get) result.push('GET');
  if (route.methods.post) result.push('POST');
  if (route.methods.put) result.push('PUT');
  if (route.methods.delete) result.push('DELETE');
  return JSON.stringify(result);
}

e_user.init(app);
e_question.init(app);
