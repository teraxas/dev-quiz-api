const store = require('../store/store');
const uuid = require('uuid');

function toUser(userRegistration) {
  return {
    uuid: uuid.v1(),
    name: userRegistration.name,
    correctAnswers: 0,
    totalAnswers: 0,
    canAddQuestion: false
  }
}

module.exports = {
  init: (expresssApp) => {
    console.log('init user endpoints');

    expresssApp.get('/user/result/:param1', (req, res) => {
      const user = store.user.get(req.params[':param1']);
      res.end(JSON.stringify(user));
    });

    expresssApp.post('/user/new', (req, res) => {
      const user = toUser(req.body);
      store.user.add(user);
      res.end(JSON.stringify(user));
    });

  }
}