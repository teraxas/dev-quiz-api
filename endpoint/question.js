const store = require('../store/store');

function toQuestion(dbObject) {
  return {
    id: dbObject.$loki,
    description: dbObject.description
  }
};

function toAnsweredQuestion(dbObject, userResponse, user) {
  return {
    id: dbObject.$loki,
    description: dbObject.description,
    explanation: dbObject.explanation,
    response: userResponse,
    result: dbObject.isTrue === userResponse
  }
};

function randomQuestion() {
  const questionsCount = store.question.count();
  const randomId = randomIntFromInterval(1, questionsCount);
  console.log(randomId);
  return store.question.get(randomId);
}

function randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

module.exports = {
  init: (expresssApp) => {
    console.log('init question endpoints');

    expresssApp.get('/question', (req, res) => {
      const question = toQuestion(randomQuestion());
      console.log('GET QUESTION', question);

      res.end(JSON.stringify(question));
    });

    expresssApp.post('/question/answer', (req, res) => {
      const question = toAnsweredQuestion(store.question.get(req.body['id']), req.body['response']);
      const user = store.user.addAnswer(req.body['userUUID'], question.result);
      question.canAddQuestion = user.canAddQuestion;

      res.end(JSON.stringify(question));
    });

    expresssApp.put('/question', (req, res) => {
      console.log('question PUT', req.body);
      const question = req.body;
      const savedQuestion = store.question.add(question);
      res.end(JSON.stringify(savedQuestion));
    });

  }
}