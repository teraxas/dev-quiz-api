const loki = require('lokijs');
const db = new loki('db.json', {
  autoload: true,
  autoloadCallback: databaseInitialize,
  autosave: true,
  autosaveInterval: 4000
});

const COLLECTIONS = {
  QUESTIONS: 'questions',
  USERS: 'users'
}

const REQUIRED_ANSWERS_TO_ADD_QUESTION = 5;

function c_questions() {
  return db.getCollection(COLLECTIONS.QUESTIONS)
};

function c_users() {
  return db.getCollection(COLLECTIONS.USERS)
};

// implement the autoloadback referenced in loki constructor
function databaseInitialize() {
  if (c_questions() === null) {
    db.addCollection(COLLECTIONS.QUESTIONS);
  }
  if (c_users() === null) {
    db.addCollection(COLLECTIONS.USERS);
  }

  printInfo();
}

function printInfo() {
  var entryCount = null; 
  console.log("number of items in questions : " + c_questions().count());
  console.log("number of items in users : " + c_users().count());
}

module.exports = {
  db: {
    serialize: function() {
      return db.serialize();
    }
  },
  question: {
    add: function(question) {
      // TODO : validate
      return c_questions().insert(question);
    },
    get: function(id) {
      return c_questions().get(id);
    },
    remove: function(id) {
      c_questions().remove(id);
    },
    count: function() {
      return c_questions().count();
    }
  },
  user: {
    add: function(user) {
      // TODO : validate
      c_users().insert(user);
    },
    get: function(uuid) {
      return c_users().findOne({'uuid': {'$eq': uuid}});
    },
    addAnswer: function(userUUID, correct) {
      const user = this.get(userUUID);
      if (correct) {
        user.correctAnswers++;
      }

      user.totalAnswers++;
      user.canAddQuestion = user.correctAnswers > REQUIRED_ANSWERS_TO_ADD_QUESTION
      c_users().update(user);
      return user;
    },
    count: function() {
      return c_users().count(); 
    }
  }
}