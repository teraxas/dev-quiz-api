const store = require('./store');

store.question.add({
    description: 'Clojure is a JVM language', 
    explanation: 'It\'s a dialect of Lisp, that runs on Java Virtual Machine', 
    isTrue: true,
    creator: 'init'
});

store.question.add({
    description: 'Kotlin is a .Net scripting language', 
    explanation: 'It\'s a null-safe JVM language', 
    isTrue: false,
    creator: 'init'
});

store.question.add({
    description: 'Webstorm is a hacking toolkit by Jetbrains', 
    explanation: 'Webstorm is actually a web development IDE based on IntelliJ', 
    isTrue: true,
    creator: 'init'
});

store.question.add({
    description: 'First software bug was literally a bug.', 
    explanation: 'A bug was found as a cause of short in electronics', 
    isTrue: true,
    creator: 'init'
});

store.question.add({
    description: 'The internet is made of cats.', 
    explanation: 'No, but really, it is.', 
    isTrue: false,
    creator: 'init'
});

store.question.add({
    description: 'Compared to other platforms, NPM has most packages uploaded per day', 
    explanation: 'Java Maven Central repository is second', 
    isTrue: true,
    creator: 'init'
});

store.question.add({
    description: 'Linux was built as a clone of NeXT OS', 
    explanation: 'It seems that Linus Torvalds wanted to have his own NeXT at first', 
    isTrue: true,
    creator: 'init'
});

store.question.add({
    description: 'Node.js uses event loop to control execution of requests', 
    explanation: 'Node.js control flow is event-driven', 
    isTrue: true,
    creator: 'init'
});
