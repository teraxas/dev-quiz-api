# DEV QUIZ API

This is API for DEV quiz app in plain Node.js.
Simple and does it's job.

## Persistance

For persistance I used an in-memory database given by the gods of Asgard - Loki.js.
It's awesomness allows fast retrieval without any surrounding infrastructure.
Bad part here is that it won't persist anything in Heroku for longer, than the lifespan of a Heroku Slug.

## Deployment

App can be deployed by a push to heroku git repository.
This happens automatically with the help of Bitbucket Pipelines, on push to `master` branch.